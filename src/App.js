import React, { Component } from 'react';
import RulesList from './components/RulesList.js'
import logo from './logo.svg';
import './App.css';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {  
      meterTitle: '',
      meterClass: '',
      rules: {
        isValidLength: null,
        hasNumber: null,
        hasUppercaseLetter: null
      }
    };
  }

  onPasswordChange(e) {
    this.setState({
      rules: {
        hasNumber: e.target.value.match(/\d/) ? true : e.target.value === '' ? null : false,
        hasUppercaseLetter: e.target.value.match(/[A-Z]/) ? true : e.target.value === '' ? null : false,
        isValidLength: e.target.value.match(/^.{6,}$/) ? true : e.target.value === '' ? null : false
      }
    }, function() {
        this.setMeterAttributes(this.state.rules);
    });
  }

  getSingleRuleStatus(status) {
    if (status) {
        return "valid";
    } else if (status === null) {
        return "null";
    }

    return "invalid";
  }

  setMeterAttributes(rules) {
    var meterLength = this.getMeterValidProperty(rules);

    switch (meterLength) {
      case 1:
        this.setState({meterClass: 'red'});
        break;
      case 2:
        this.setState({meterClass: 'yellow'});
        break;
      case 3:
        this.setState({meterClass: 'green'});
        break;

      default:
        this.setState({meterClass: ''});
        break;
    }
  }

  getMeterValidProperty(rules) {
    var valid_property_count = 0, property;
    for (property in rules) {
      if (rules[property]) {
          valid_property_count = valid_property_count + 1;
      }
    }
    return valid_property_count;
  }

  render() {
    return (
      <div className="signup-wrapper">
        <div className="signup-box">
          <div className="signup-header">
            <img className="signup-brand" src={logo} alt="logo-olist"></img>
            <p className="signup-main-text">Crie sua conta</p>
          </div>

          <form className="signup-form" onSubmit={this.handleSubmit}>
            <label className="signup-form-label" htmlFor="name">
              Nome completo
              <input className="signup-form-input" type="text" name="name" />
            </label>
            <label className="signup-form-label" htmlFor="email">
              E-mail
              <input className="signup-form-input" type="text" name="email" />
            </label>
            <label className="signup-form-label" htmlFor="password">
              Senha
              <input className={this.state.meterClass + ' signup-form-input signup-password-input'} type="password" name="password" placeholder="Digite sua senha" onChange={this.onPasswordChange.bind(this)} />
            </label>

            <RulesList
              isValidLength={this.getSingleRuleStatus(this.state.rules.isValidLength)}
              hasNumber={this.getSingleRuleStatus(this.state.rules.hasNumber)}
              hasUppercaseLetter={this.getSingleRuleStatus(this.state.rules.hasUppercaseLetter)}
              className={this.state.meterClass}
            />

            <label className="signup-form-label" htmlFor="confirm-password">
              Confirme sua senha
              <input className="signup-form-input" type="password" name="confirm-password" />
            </label>

            <input className="signup-form-submit" type="submit" value="Criar conta" />
          </form>
        </div>
      </div>
    )
  }
}

export default App;
