
import React, { Component } from 'react';

class RulesList extends Component {
  render(){
    return (
      <div className="rules-wrapper">
      
        <div className={this.props.className + ' meter-wrapper'}>
          <div className="meter-item"></div>
          <div className="meter-item"></div>
          <div className="meter-item"></div>
        </div>

        <ul className="rules-list">
          <li className={this.props.isValidLength}>
            Pelo menos 6 caracteres
          </li>
          <li className={this.props.hasUppercaseLetter}>
            Pelo menos 1 letra maiúscula
          </li>
          <li className={this.props.hasNumber}>
            Pelo menos 1 número
          </li>
        </ul>
      </div>
    )
  }
}

export default RulesList;